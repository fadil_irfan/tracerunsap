<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldColumnToDataKuesionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_kuesioners', function (Blueprint $table) {
            $table->string('nimhsmsmh')->nullable()->change();
            $table->string('kdptimsmh')->nullable()->change();
            $table->string('tahun_lulus')->nullable()->change();
            $table->string('kdpstmsmh')->nullable()->change();
            $table->string('nmmhsmsmh')->nullable()->change();
            $table->string('telpomsmh')->nullable()->change();
            $table->string('emailmsmh')->nullable()->change();
            $table->string('nik')->nullable()->change();
            $table->string('npwp')->nullable()->change();
            $table->integer('f8')->nullable()->change();
            $table->integer('f504')->nullable()->change();
            $table->integer('f502')->nullable()->change();
            $table->integer('f506')->nullable()->change();
            $table->integer('f1101')->nullable()->change();
            $table->integer('f5c')->nullable()->change();
            $table->integer('f5d')->nullable()->change();
            $table->integer('f18a')->nullable()->change();
            $table->string('f18b')->nullable()->change();
            $table->string('f18c')->nullable()->change();
            $table->date('f18d')->nullable()->change();
            $table->integer('f1201')->nullable()->change();
            $table->integer('f14')->nullable()->change();
            $table->integer('f15')->nullable()->change();
            $table->integer('f1761')->nullable()->change();
            $table->integer('f1762')->nullable()->change();
            $table->integer('f1763')->nullable()->change();
            $table->integer('f1764')->nullable()->change();
            $table->integer('f1765')->nullable()->change();
            $table->integer('f1766')->nullable()->change();
            $table->integer('f1767')->nullable()->change();
            $table->integer('f1768')->nullable()->change();
            $table->integer('f1769')->nullable()->change();
            $table->integer('f1770')->nullable()->change();
            $table->integer('f1771')->nullable()->change();
            $table->integer('f1772')->nullable()->change();
            $table->integer('f1773')->nullable()->change();
            $table->integer('f1774')->nullable()->change();
            $table->integer('f21')->nullable()->change();
            $table->integer('f22')->nullable()->change();
            $table->integer('f23')->nullable()->change();
            $table->integer('f24')->nullable()->change();
            $table->integer('f25')->nullable()->change();
            $table->integer('f26')->nullable()->change();
            $table->integer('f27')->nullable()->change();
            $table->integer('f301')->nullable()->change();
            $table->integer('f401')->nullable()->change();
            $table->integer('f402')->nullable()->change();
            $table->integer('f403')->nullable()->change();
            $table->integer('f404')->nullable()->change();
            $table->integer('f405')->nullable()->change();
            $table->integer('f406')->nullable()->change();
            $table->integer('f407')->nullable()->change();
            $table->integer('f408')->nullable()->change();
            $table->integer('f409')->nullable()->change();
            $table->integer('f410')->nullable()->change();
            $table->integer('f411')->nullable()->change();
            $table->integer('f412')->nullable()->change();
            $table->integer('f413')->nullable()->change();
            $table->integer('f414')->nullable()->change();
            $table->integer('f415')->nullable()->change();
            $table->integer('f6')->nullable()->change();
            $table->integer('f7')->nullable()->change();
            $table->integer('f7a')->nullable()->change();
            $table->integer('f1001')->nullable()->change();
            $table->integer('f1601')->nullable()->change();
            $table->integer('f1602')->nullable()->change();
            $table->integer('f1603')->nullable()->change();
            $table->integer('f1604')->nullable()->change();
            $table->integer('f1605')->nullable()->change();
            $table->integer('f1606')->nullable()->change();
            $table->integer('f1607')->nullable()->change();
            $table->integer('f1608')->nullable()->change();
            $table->integer('f1609')->nullable()->change();
            $table->integer('f1610')->nullable()->change();
            $table->integer('f1611')->nullable()->change();
            $table->integer('f1612')->nullable()->change();
            $table->integer('f1613')->nullable()->change();
            $table->string('f5a1', 2)->nullable()->change();
            $table->string('f5a2', 4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_kuesioners', function (Blueprint $table) {
            //
        });
    }
}
