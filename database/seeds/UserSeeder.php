<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserLevelSeeder::class);
        DB::table('users')->insert([
            'username' => 'muhamadiqbalriv',
            'fullname' => 'Muhamad Iqbal Rivaldi',
            'email' => 'muhamadiqbalrivaldi03@gmail.com',
            'gender' => 'Laki-laki',
            'id_user_level' => 1,
            'password' => Hash::make('password'),
        ]);
    }
}
