<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracer extends Model
{
    protected $table = 'data_tracers';

    protected $guarded = ['id'];
    
}
