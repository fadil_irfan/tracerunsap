<?php

namespace App\Exports;

use App\Tracer;
use Maatwebsite\Excel\Concerns\FromCollection;

class TracersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tracer::all();
    }
}
