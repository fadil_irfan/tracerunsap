<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kuesioner;
use App\Models\Province;
use App\Models\Regency;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Auth;

class KuesionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index','create','regencies', 'store']);
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect(route('kuesioners.create'));die;
        }
        $data['title'] = "Data Kuesioner";
        $data['kuesioners'] = Kuesioner::all();

        return view('back.kuesioners.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Tambah Kuesioner";
        $data['provincies'] = Province::all();
        
        return view('back.kuesioners.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nimhsmsmh' => 'required',
            'kdptimsmh' => 'required',
            'tahun_lulus' => 'required',
            'kdpstmsmh' => 'required',
            'nmmhsmsmh' => 'required',
            'telpomsmh' => 'required',
            'whatsapp' => 'required',
            'emailmsmh' => 'required',
            'nik' => 'required',
        ]);

        $kuesioner = new Kuesioner($request->all());

        $kuesioner->save();
        if (Auth::check()) {
            return redirect('/kuesioners')->with('success', 'Data berhasil ditambahkan!');
        }else{
            return redirect()->route('kuesioners.create', ['nama_lengkap' => $request->get('nmmhsmsmh')])->with('success', 'Terimakasih banyak sudah mengisi formulir, untuk informasi selanjutnya nanti akan dihubungi melalui no telp/whatsapp terdaftar. Untuk informasi lainnya terkait tracer study bisa menghubungi melalui whatsapp +6285220717928 (Irfan Fadil)');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Edit Kuesioner";
        $data['detail'] = Kuesioner::findOrFail($id)->first();
        $data['provincies'] = Province::all();
        $data['regencies'] = Regency::where('province_id','=',$data['detail']->f5a1)->get();
        
        return view('back.kuesioners.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nimhsmsmh' => 'required',
            'kdptimsmh' => 'required',
            'tahun_lulus' => 'required',
            'kdpstmsmh' => 'required',
            'nmmhsmsmh' => 'required',
            'telpomsmh' => 'required',
            'whatsapp' => 'required',
            'emailmsmh' => 'required',
            'nik' => 'required',
        ]);

        $kuesioner = Kuesioner::findOrFail($id);
        $data = $request->all();

        $kuesioner->update($data);

        return redirect('/kuesioners')->with('success', 'Data berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kuesioner = Kuesioner::findOrFail($id);

        if ($kuesioner->delete()) {
            return redirect('/kuesioners')->with('success', 'Data berhasil dihapus!');
        }else{
            return redirect('/kuesioners')->with('error', 'Data gagal dihapus!');
        }
    }
 
    function regencies(Request $request){
        $result = Regency::where('province_id', '=', $request->id)->get();
        echo json_encode($result);
    }

    public function export() 
    {
        return Excel::download(new KuesionersExport, 'kuesioners.xlsx');
    }
}

