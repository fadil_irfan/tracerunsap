<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserLevel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Data User";
        $data['users'] = User::all();

        return view('back.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Tambah User";
        $data['user_levels'] = UserLevel::get();
        
        return view('back.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'username' => 'required|unique:users|max:20|min:6|alpha_dash',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
            'email' => 'required|unique:users',
            'gender' => 'required',
            'id_user_level' => 'required',
            'phone' => 'required|unique:users|digits_between:9,16|numeric',
            'birthday' => 'nullable',
            'picture' => 'mimes:jpeg,bmp,png,jpg|max:2048'
        ]);

        $path = ($request->picture)
        ? $request->file('picture')->store("/public/input/users/profile")
        : null;

        $user = new User([
            'fullname' => $request->get('fullname'),
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'email' => $request->get('email'),
            'gender' => $request->get('gender'),
            'phone' => $request->get('phone'),
            'birthday' => $request->get('birthday'),
            'id_user_level' => $request->get('id_user_level'),
            'picture' => $path
        ]);

        $user->save();
        return redirect('/users')->with('success', 'Pengguna berhasil ditambahkan!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Edit User";
        $data['detail'] = User::findOrFail($id);
        $data['user_levels'] = UserLevel::get();

        return view('back.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fullname' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'username' => 'required|max:20|min:6|alpha_dash|unique:users,username,'.$id,
            'email' => 'required|unique:users,email,'.$id,
            'gender' => 'required',
            'id_user_level' => 'required',
            'phone' => 'required|digits_between:9,16|numeric|unique:users,phone,'.$id,
            'birthday' => 'nullable',
            'picture' => 'mimes:jpeg,bmp,png,jpg|max:2048'
        ]);
            
        $user = User::findOrFail($id);
        $path = ($request->picture)
        ? $request->file('picture')->store("/public/input/users/profile")
        : null;


        if ($request->picture) {
            Storage::delete($user->picture);
        }

        $user->fullname =  $request->get('fullname');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->gender = $request->get('gender');
        $user->birthday = $request->get('birthday');
        $user->id_user_level = $request->get('id_user_level');
        if ($path != null) {
            $user->picture = $path;
        }
        $user->save();

        return redirect('/users')->with('success', 'Pengguna berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if (Storage::exists($user->picture)) {
            Storage::delete($user->picture);
        }
        if ($user->delete()) {
            return redirect('/users')->with('success', 'Pengguna berhasil dihapus!');
        }else{
            return redirect('/users')->with('error', 'Pengguna gagal dihapus!');
        }
    }

    public function changePassword(Request $request, $id){

        $user = User::findOrFail($id);

        $request->validate([
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:new_password'
        ]);

        if (!Hash::check($request->old_password, $user->password)) {
            return redirect(route('users.edit', $id))->with('error', 'Password lama anda salah!');
        }

        $user->password = Hash::make($request->new_password);

        if ($user->save()) {
            return redirect(route('users.edit', $id))->with('success', 'Password berhasil diperbaharui!');
        }else{
            return redirect(route('users.edit', $id))->with('error', 'Password gagal diperbaharui!');
        }

    }
}
