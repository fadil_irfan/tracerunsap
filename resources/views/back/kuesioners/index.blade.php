@extends('layouts.back')
@section('css')
    <link href="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/back/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" /> 
@endsection
@section('js')
    <!-- datatable js -->
    <script src="{{asset('assets/back/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.bootstrap4.min.j')}}s"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/back/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/back/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('assets/back/libs/datatables/dataTables.select.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>
    <!-- Datatables init -->
    <script src="{{asset('assets/back/js/pages/datatables.init.js')}}"></script>
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kuesioner</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Kuesioner</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="header-title mt-0 mb-1">Data Kuesioner</h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{route('kuesioners.create')}}" class="btn btn-primary btn-xs" style="margin-bottom: 10px;">Tambah Data Kuesioner</a>
                                </div>
                            </div>
                            @if ($msg = Session::get('success'))
                                <div class="alert alert-success">
                                    {{$msg}}
                                </div>
                            @endif
                            @if ($msg = Session::get('error'))
                                <div class="alert alert-danger">
                                    {{$msg}}
                                </div>
                            @endif
                            <p class="sub-header text-right">
                            </p>


                            <table id="example" class="table table-striped dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th>Kode Perguruan Tinggi Mahasiswa</th>
                                        <th>Kode PST</th>
                                        <th>NIM Mahasiswa</th>
                                        <th>Nama Mahasiswa</th>
                                        <th>Telepon Mahasiswa</th>
                                        <th>Email Mahasiswa</th>
                                        <th>Tahun Lulus</th>
                                        <th>NIK</th>
                                        <th>NPWP</th>
                                        <th>f8</th>
                                        <th>f504</th>
                                        <th>f502</th>
                                        <th>f505</th>
                                        <th>f506</th>
                                        <th>f5a1</th>
                                        <th>f5a2</th>
                                        <th>f1101</th>
                                        <th>f1102</th>
                                        <th>f5b</th>
                                        <th>f5c</th>
                                        <th>f5d</th>
                                        <th>f18a</th>
                                        <th>f18b</th>
                                        <th>f18c</th>
                                        <th>f18d</th>
                                        <th>f1201</th>
                                        <th>f1202</th>
                                        <th>f14</th>
                                        <th>f15</th>
                                        <th>f1761</th>
                                        <th>f1762</th>
                                        <th>f1763</th>
                                        <th>f1764</th>
                                        <th>f1765</th>
                                        <th>f1766</th>
                                        <th>f1767</th>
                                        <th>f1768</th>
                                        <th>f1769</th>
                                        <th>f1770</th>
                                        <th>f1771</th>
                                        <th>f1772</th>
                                        <th>f1773</th>
                                        <th>f1774</th>
                                        <th>f21</th>
                                        <th>f22</th>
                                        <th>f23</th>
                                        <th>f24</th>
                                        <th>f25</th>
                                        <th>f26</th>
                                        <th>f27</th>
                                        <th>f301</th>
                                        <th>f302</th>
                                        <th>f303</th>
                                        <th>f401</th>
                                        <th>f402</th>
                                        <th>f403</th>
                                        <th>f404</th>
                                        <th>f405</th>
                                        <th>f406</th>
                                        <th>f407</th>
                                        <th>f408</th>
                                        <th>f409</th>
                                        <th>f410</th>
                                        <th>f411</th>
                                        <th>f412</th>
                                        <th>f413</th>
                                        <th>f414</th>
                                        <th>f415</th>
                                        <th>f416</th>
                                        <th>f6</th>
                                        <th>f7</th>
                                        <th>f7a</th>
                                        <th>f1001</th>
                                        <th>f1002</th>
                                        <th>f1601</th>
                                        <th>f1602</th>
                                        <th>f1603</th>
                                        <th>f1604</th>
                                        <th>f1605</th>
                                        <th>f1606</th>
                                        <th>f1607</th>
                                        <th>f1608</th>
                                        <th>f1609</th>
                                        <th>f1610</th>
                                        <th>f1611</th>
                                        <th>f1612</th>
                                        <th>f1613</th>
                                        <th>f1614</th>
                                        @if (Auth::user()->user_level->name == 'Administrator')
                                            <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                            
                            
                                <tbody>
                                    @foreach ($kuesioners as $i)
                                        <tr>
                                            <td>{{$i->kdptimsmh}}</td>
                                            <td>{{$i->kdpstmsmh}}T</td>
                                            <td>{{$i->nimhsmsmh}}</td>
                                            <td>{{$i->nmmhsmsmh}}</td>
                                            <td>{{$i->telpomsmh}}</td>
                                            <td>{{$i->emailmsmh}}</td>
                                            <td>{{$i->tahun_lulus}}</td>
                                            <td>{{$i->nik}}</td>
                                            <td>{{$i->npwp}}</td>
                                            <td>{{$i->f8}}</td>
                                            <td>{{$i->f504}}</td>
                                            <td>{{$i->f502}}</td>
                                            <td>{{$i->f505}}</td>
                                            <td>{{$i->f506}}</td>
                                            <td>{{$i->f5a1}}</td>
                                            <td>{{$i->f5a2}}</td>
                                            <td>{{$i->f1101}}</td>
                                            <td>{{$i->f1102}}</td>
                                            <td>{{$i->f5b}}</td>
                                            <td>{{$i->f5c}}</td>
                                            <td>{{$i->f5d}}</td>
                                            <td>{{$i->f18a}}</td>
                                            <td>{{$i->f18b}}</td>
                                            <td>{{$i->f18c}}</td>
                                            <td>{{$i->f18d}}</td>
                                            <td>{{$i->f1201}}</td>
                                            <td>{{$i->f1202}}</td>
                                            <td>{{$i->f14}}</td>
                                            <td>{{$i->f15}}</td>
                                            <td>{{$i->f1761}}</td>
                                            <td>{{$i->f1762}}</td>
                                            <td>{{$i->f1763}}</td>
                                            <td>{{$i->f1764}}</td>
                                            <td>{{$i->f1765}}</td>
                                            <td>{{$i->f1766}}</td>
                                            <td>{{$i->f1767}}</td>
                                            <td>{{$i->f1768}}</td>
                                            <td>{{$i->f1769}}</td>
                                            <td>{{$i->f1770}}</td>
                                            <td>{{$i->f1771}}</td>
                                            <td>{{$i->f1772}}</td>
                                            <td>{{$i->f1773}}</td>
                                            <td>{{$i->f1774}}</td>
                                            <td>{{$i->f21}}</td>
                                            <td>{{$i->f22}}</td>
                                            <td>{{$i->f23}}</td>
                                            <td>{{$i->f24}}</td>
                                            <td>{{$i->f25}}</td>
                                            <td>{{$i->f26}}</td>
                                            <td>{{$i->f27}}</td>
                                            <td>{{$i->f301}}</td>
                                            <td>{{$i->f302}}</td>
                                            <td>{{$i->f303}}</td>
                                            <td>{{$i->f401}}</td>
                                            <td>{{$i->f402}}</td>
                                            <td>{{$i->f403}}</td>
                                            <td>{{$i->f404}}</td>
                                            <td>{{$i->f405}}</td>
                                            <td>{{$i->f406}}</td>
                                            <td>{{$i->f407}}</td>
                                            <td>{{$i->f408}}</td>
                                            <td>{{$i->f409}}</td>
                                            <td>{{$i->f410}}</td>
                                            <td>{{$i->f411}}</td>
                                            <td>{{$i->f412}}</td>
                                            <td>{{$i->f413}}</td>
                                            <td>{{$i->f414}}</td>
                                            <td>{{$i->f415}}</td>
                                            <td>{{$i->f416}}</td>
                                            <td>{{$i->f6}}</td>
                                            <td>{{$i->f7}}</td>
                                            <td>{{$i->f7a}}</td>
                                            <td>{{$i->f1001}}</td>
                                            <td>{{$i->f1002}}</td>
                                            <td>{{$i->f1601}}</td>
                                            <td>{{$i->f1602}}</td>
                                            <td>{{$i->f1603}}</td>
                                            <td>{{$i->f1604}}</td>
                                            <td>{{$i->f1605}}</td>
                                            <td>{{$i->f1606}}</td>
                                            <td>{{$i->f1607}}</td>
                                            <td>{{$i->f1608}}</td>
                                            <td>{{$i->f1609}}</td>
                                            <td>{{$i->f1610}}</td>
                                            <td>{{$i->f1611}}</td>
                                            <td>{{$i->f1612}}</td>
                                            <td>{{$i->f1613}}</td>
                                            <td>{{$i->f1614}}</td>
                                            @if (Auth::user()->user_level->name == 'Administrator')
                                            <td>
                                                <a href="{{ route('kuesioners.edit', $i->id) }}" class="btn btn-rounded btn-info btn-xs"><i class="anticon anticon-edit"></i> Edit</a>
                                                <form action="{{ route('kuesioners.destroy', $i->id) }}" method="post"
                                                    onsubmit="return confirm('Yakin hapus data ini?')">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-rounded btn-danger btn-xs" style="margin: 5px auto;">
                                                        <i class="anticon anticon-delete"></i> Hapus</button>
                                                </form>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->

        </div> <!-- container-fluid -->

    </div> <!-- content -->

@endsection